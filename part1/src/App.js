import './App.css';
import Message from './components/Message';

function App() {
  return (
    <>
      <Message color="#AF1ACA" message="Hello word" />
      <Message color="#72A2AC" message="Sitio web en desarrollo" />
      <Message color="#212121" message="Proximamente" />
    </>
  );
}

export default App;
