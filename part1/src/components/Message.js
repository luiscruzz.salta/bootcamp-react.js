const Message = (props) => {
    return <h3
        style={{ color: props.color }}>
        {props.message}
    </h3>
}

export default Message;